import django
from django.utils.timezone import now
from django.db import models
# default=timezone.now


class ProvinceInfo(models.Model):
    """省信息"""
    province_number = models.IntegerField(verbose_name="省编号")
    name = models.CharField(verbose_name="省", max_length=255, default="某省")
    created_at = models.DateTimeField(verbose_name="创建时间", auto_created=True, default=now)
    updated_at = models.DateTimeField(verbose_name="更新时间", auto_now=True)
    # date_created = models.DateTimeField(verbose_name="创建时间", default=timezone.now)
    # updated_at = models.DateTimeField(verbose_name="更新时间", default = '', blank = True, null = True)

    class Meta:
        db_table = 'in_province_info'
        unique_together = ["province_number", "name"]
        verbose_name = "省信息表"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class CityInfo(models.Model):
    """城市信息"""
    city_number = models.IntegerField(verbose_name="城市编号")
    name = models.CharField(verbose_name="市", max_length=255, default="某市")

    province_number = models.IntegerField(verbose_name="所属省编号")
    province_name = models.CharField(verbose_name="所属省名", max_length=255, default="某省")

    created_at = models.DateTimeField(verbose_name="创建时间", auto_created=True, default=now)
    updated_at = models.DateTimeField(verbose_name="更新时间", auto_now=True)

    class Meta:
        db_table = 'in_city_info'
        unique_together = ["city_number", "name"]
        verbose_name = "城市信息表"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class CountyInfo(models.Model):
    """区县信息"""
    county_number = models.IntegerField(verbose_name="区县编号")
    name = models.CharField(verbose_name="区/县", max_length=255, default="某区")

    city_number = models.IntegerField(verbose_name="所属市编号")
    city_name = models.CharField(verbose_name="所属市名", max_length=255, default="某市")

    created_at = models.DateTimeField(verbose_name="创建时间", auto_created=True, default=now)
    updated_at = models.DateTimeField(verbose_name="更新时间", auto_now=True)

    class Meta:
        db_table = 'in_county_info'
        unique_together = ["county_number", "name"]
        verbose_name = "区县信息表"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class StationInfo(models.Model):
    """供电服务所（站）信息"""
    station_number = models.IntegerField(verbose_name="供电服务所（站）编号")
    name = models.CharField(verbose_name="供电服务所（站）", max_length=255, default="某供电服务所（站）")
    county_number = models.IntegerField(verbose_name="所属区县编号")
    county_name = models.CharField(verbose_name="所属区县名", max_length=255, default="某区县")

    is_active = models.IntegerField(verbose_name="运行状态", default=1)
    created_at = models.DateTimeField(verbose_name="创建时间", auto_created=True, default=now)
    updated_at = models.DateTimeField(verbose_name="更新时间", auto_now=True)

    class Meta:
        db_table = 'in_station_info'
        unique_together = ["station_number", "name"]
        verbose_name = "供电服务所（站）信息表"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class ManagerInfo(models.Model):
    """台区管理员信息"""
    manager_number = models.IntegerField(verbose_name="管理员编号")
    name = models.CharField(verbose_name="管理员", max_length=255, default="某管理员")

    station_number = models.IntegerField(verbose_name="所属供电服务所（站）编号")
    station_name = models.CharField(verbose_name="所属供电服务所（站）名", max_length=255, default="某供电服务所（站）")

    county_number = models.IntegerField(verbose_name="所属区县编号", blank=True, null=True)
    county_name = models.CharField(verbose_name="所属区县名", max_length=255, default="某区县")

    is_active = models.IntegerField(verbose_name="运行状态", default=1)
    created_at = models.DateTimeField(verbose_name="创建时间", auto_created=True, default=now)
    updated_at = models.DateTimeField(verbose_name="更新时间", auto_now=True)

    class Meta:
        db_table = 'in_manager_info'
        unique_together = ["manager_number", "name"]
        verbose_name = "台区管理员信息表"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class TransformerInfo(models.Model):
    """台区信息"""
    transformer_number = models.IntegerField(verbose_name="台区编号")
    name = models.CharField(verbose_name="台区名称", max_length=255, default="某台区")
    multiplying_power = models.IntegerField(verbose_name="台区倍率", default=200)
    manager_number = models.IntegerField(verbose_name="所属管理员编号")
    manager_name = models.CharField(verbose_name="所属管理员名", max_length=255, default="某管理员")

    station_number = models.IntegerField(verbose_name="所属供电服务所（站）编号")
    station_name = models.CharField(verbose_name="所属供电服务所（站）名", max_length=255, default="某供电服务所（站）")

    county_number = models.IntegerField(verbose_name="所属区县编号", blank=True, null=True)
    county_name = models.CharField(verbose_name="所属区县名", max_length=255, default="某区县")

    household_count = models.IntegerField(verbose_name="用户数", blank=True, null=True)
    volume = models.IntegerField(verbose_name="额定总容量", blank=True, null=True)
    volume_per_household = models.DecimalField(verbose_name="户均容量", blank=True, null=True, max_digits=10, decimal_places=2)
    commission_date = models.DateTimeField(verbose_name="投运日期", blank=True, null=True)
    is_HPLC = models.SmallIntegerField(verbose_name="是否为高速宽带电力线载波通信", default=0)
    tag = models.TextField(verbose_name="台区标签", blank=True)
    is_active = models.IntegerField(verbose_name="运行状态", default=1)
    created_at = models.DateTimeField(verbose_name="创建时间", auto_created=True, default=now)
    updated_at = models.DateTimeField(verbose_name="更新时间", auto_now=True)

    # added 1230
    load_max_rate = models.DecimalField(verbose_name="最大负载率", blank=True, null=True, max_digits=10, decimal_places=2)
    load_growth_rate = models.DecimalField(verbose_name="负荷自然增长率", blank=True, null=True, max_digits=10, decimal_places=2)
    peak_valley_difference = models.DecimalField(verbose_name="负荷谷峰差", blank=True, null=True, max_digits=8,
                                                 decimal_places=2)
    r_percentage = models.DecimalField(verbose_name="空调负荷占比", blank=True, null=True, max_digits=8, decimal_places=2)
    load_status = models.CharField(verbose_name="重过载状态", max_length=255, default="正常")
    load_status_forecast_rate = models.DecimalField(verbose_name="预警准确率", blank=True, null=True, max_digits=10,
                                                    decimal_places=2)
    load_forecast_rate = models.DecimalField(verbose_name="负荷预测准确率", blank=True, null=True, max_digits=10, decimal_places=2)

    group_name = models.CharField(verbose_name="台区群组", max_length=255, default="某台区群组")
    group_number = models.IntegerField(verbose_name="台区群组编号", default=0)

    class Meta:
        db_table = 'in_transformer_info'
        unique_together = ["transformer_number", "name"]
        verbose_name = "台区信息表"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class ProvinceLoadMonthly(models.Model):
    """省月数据"""
    province_number = models.IntegerField(verbose_name="省编号")

    month = models.CharField(verbose_name="月份", max_length=255)
    normal_load_count = models.IntegerField(verbose_name="正常台区数量", blank=True, null=True)
    heavy_load_count = models.IntegerField(verbose_name="重载台区数量", blank=True, null=True)
    over_load_count = models.IntegerField(verbose_name="过载台区数量", blank=True, null=True)
    over_load_0_count = models.IntegerField(verbose_name="轻度过载台区数量", blank=True, null=True)
    over_load_1_count = models.IntegerField(verbose_name="中度过载台区数量", blank=True, null=True)
    over_load_2_count = models.IntegerField(verbose_name="重度过载台区数量", blank=True, null=True)
    over_load_3_count = models.IntegerField(verbose_name="异常过载台区数量", blank=True, null=True)

    normal_load_count_f = models.IntegerField(verbose_name="正常台区预测数量", blank=True, null=True)
    heavy_load_count_f = models.IntegerField(verbose_name="重载台区预测数量", blank=True, null=True)
    over_load_count_f = models.IntegerField(verbose_name="过载台区预测数量", blank=True, null=True)
    over_load_0_count_f = models.IntegerField(verbose_name="轻度过载台区预测数量", blank=True, null=True)
    over_load_1_count_f = models.IntegerField(verbose_name="中度过载台区预测数量", blank=True, null=True)
    over_load_2_count_f = models.IntegerField(verbose_name="重度过载台区预测数量", blank=True, null=True)
    over_load_3_count_f = models.IntegerField(verbose_name="异常过载台区预测数量", blank=True, null=True)

    load_average = models.DecimalField(verbose_name="平均负荷实际值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_average_f = models.DecimalField(verbose_name="平均负荷预测值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_max = models.DecimalField(verbose_name="最大负荷实际值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_max_f = models.DecimalField(verbose_name="最大负荷预测值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_min = models.DecimalField(verbose_name="最小负荷实际值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_min_f = models.DecimalField(verbose_name="最小负荷预测值", blank=True, null=True, max_digits=10, decimal_places=2)

    load_rate_average = models.DecimalField(verbose_name="平均负荷率", blank=True, null=True, max_digits=8, decimal_places=2)
    load_rate_min_average = models.DecimalField(verbose_name="平均最小负荷率", blank=True, null=True, max_digits=8, decimal_places=2)

    load_rate_average_f = models.DecimalField(verbose_name="平均负荷率预测值", blank=True, null=True, max_digits=8, decimal_places=2)
    load_rate_min_average_f = models.DecimalField(verbose_name="平均最小负荷率预测值", blank=True, null=True, max_digits=8,
                                                decimal_places=2)

    MOM_energy = models.DecimalField(verbose_name="用电量月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    MOM_energy_f = models.DecimalField(verbose_name="用电量预测值月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_energy = models.DecimalField(verbose_name="用电量年同比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_energy_f = models.DecimalField(verbose_name="用电量预测值年同比", blank=True, null=True, max_digits=8, decimal_places=2)

    MOM_load_average = models.DecimalField(verbose_name="平均负荷月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    MOM_load_average_f = models.DecimalField(verbose_name="平均负荷预测值月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_average = models.DecimalField(verbose_name="平均负荷年同比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_average_f = models.DecimalField(verbose_name="平均负荷预测值年同比", blank=True, null=True, max_digits=8, decimal_places=2)

    MOM_load_max = models.DecimalField(verbose_name="最大负荷月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    MOM_load_max_f = models.DecimalField(verbose_name="最大负荷预测值月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_max = models.DecimalField(verbose_name="最大负荷年同比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_max_f = models.DecimalField(verbose_name="最大负荷预测值年同比", blank=True, null=True, max_digits=8, decimal_places=2)

    MOM_load_min = models.DecimalField(verbose_name="最小负荷月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    MOM_load_min_f = models.DecimalField(verbose_name="最小负荷预测值月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_min = models.DecimalField(verbose_name="最小负荷年同比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_min_f = models.DecimalField(verbose_name="最小负荷预测值年同比", blank=True, null=True, max_digits=8, decimal_places=2)

    class Meta:
        db_table = 'sys_province_monthly'
        unique_together = ["month", "province_number"]
        verbose_name = "省月数据表"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class CityLoadMonthly(models.Model):
    """城市月数据"""
    city_number = models.IntegerField(verbose_name="城市编号")
    province_number = models.IntegerField(verbose_name="所属省编号")

    month = models.CharField(verbose_name="月份", max_length=255)
    normal_load_count = models.IntegerField(verbose_name="正常台区数量", blank=True, null=True)
    heavy_load_count = models.IntegerField(verbose_name="重载台区数量", blank=True, null=True)
    over_load_count = models.IntegerField(verbose_name="过载台区数量", blank=True, null=True)
    over_load_0_count = models.IntegerField(verbose_name="轻度过载台区数量", blank=True, null=True)
    over_load_1_count = models.IntegerField(verbose_name="中度过载台区数量", blank=True, null=True)
    over_load_2_count = models.IntegerField(verbose_name="重度过载台区数量", blank=True, null=True)
    over_load_3_count = models.IntegerField(verbose_name="异常过载台区数量", blank=True, null=True)

    normal_load_count_f = models.IntegerField(verbose_name="正常台区预测数量", blank=True, null=True)
    heavy_load_count_f = models.IntegerField(verbose_name="重载台区预测数量", blank=True, null=True)
    over_load_count_f = models.IntegerField(verbose_name="过载台区预测数量", blank=True, null=True)
    over_load_0_count_f = models.IntegerField(verbose_name="轻度过载台区预测数量", blank=True, null=True)
    over_load_1_count_f = models.IntegerField(verbose_name="中度过载台区预测数量", blank=True, null=True)
    over_load_2_count_f = models.IntegerField(verbose_name="重度过载台区预测数量", blank=True, null=True)
    over_load_3_count_f = models.IntegerField(verbose_name="异常过载台区预测数量", blank=True, null=True)

    load_average = models.DecimalField(verbose_name="平均负荷实际值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_average_f = models.DecimalField(verbose_name="平均负荷预测值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_max = models.DecimalField(verbose_name="最大负荷实际值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_max_f = models.DecimalField(verbose_name="最大负荷预测值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_min = models.DecimalField(verbose_name="最小负荷实际值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_min_f = models.DecimalField(verbose_name="最小负荷预测值", blank=True, null=True, max_digits=10, decimal_places=2)

    load_rate_average = models.DecimalField(verbose_name="平均负荷率", blank=True, null=True, max_digits=8, decimal_places=2)
    load_rate_min_average = models.DecimalField(verbose_name="平均最小负荷率", blank=True, null=True, max_digits=8, decimal_places=2)

    load_rate_average_f = models.DecimalField(verbose_name="平均负荷率预测值", blank=True, null=True, max_digits=8, decimal_places=2)
    load_rate_min_average_f = models.DecimalField(verbose_name="平均最小负荷率预测值", blank=True, null=True, max_digits=8,
                                                decimal_places=2)

    MOM_energy = models.DecimalField(verbose_name="用电量月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    MOM_energy_f = models.DecimalField(verbose_name="用电量预测值月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_energy = models.DecimalField(verbose_name="用电量年同比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_energy_f = models.DecimalField(verbose_name="用电量预测值年同比", blank=True, null=True, max_digits=8, decimal_places=2)

    MOM_load_average = models.DecimalField(verbose_name="平均负荷月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    MOM_load_average_f = models.DecimalField(verbose_name="平均负荷预测值月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_average = models.DecimalField(verbose_name="平均负荷年同比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_average_f = models.DecimalField(verbose_name="平均负荷预测值年同比", blank=True, null=True, max_digits=8, decimal_places=2)

    MOM_load_max = models.DecimalField(verbose_name="最大负荷月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    MOM_load_max_f = models.DecimalField(verbose_name="最大负荷预测值月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_max = models.DecimalField(verbose_name="最大负荷年同比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_max_f = models.DecimalField(verbose_name="最大负荷预测值年同比", blank=True, null=True, max_digits=8, decimal_places=2)

    MOM_load_min = models.DecimalField(verbose_name="最小负荷月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    MOM_load_min_f = models.DecimalField(verbose_name="最小负荷预测值月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_min = models.DecimalField(verbose_name="最小负荷年同比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_min_f = models.DecimalField(verbose_name="最小负荷预测值年同比", blank=True, null=True, max_digits=8, decimal_places=2)

    class Meta:
        db_table = 'sys_city_monthly'
        unique_together = ["month", "city_number"]
        verbose_name = "城市月数据表"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class CountyLoadMonthly(models.Model):
    """区县月数据"""
    county_number = models.IntegerField(verbose_name="区县编号")
    city_number = models.IntegerField(verbose_name="所属城市编号")

    month = models.CharField(verbose_name="月份", max_length=255)
    normal_load_count = models.IntegerField(verbose_name="正常台区数量", blank=True, null=True)
    heavy_load_count = models.IntegerField(verbose_name="重载台区数量", blank=True, null=True)
    over_load_count = models.IntegerField(verbose_name="过载台区数量", blank=True, null=True)
    over_load_0_count = models.IntegerField(verbose_name="轻度过载台区数量", blank=True, null=True)
    over_load_1_count = models.IntegerField(verbose_name="中度过载台区数量", blank=True, null=True)
    over_load_2_count = models.IntegerField(verbose_name="重度过载台区数量", blank=True, null=True)
    over_load_3_count = models.IntegerField(verbose_name="异常过载台区数量", blank=True, null=True)

    normal_load_count_f = models.IntegerField(verbose_name="正常台区预测数量", blank=True, null=True)
    heavy_load_count_f = models.IntegerField(verbose_name="重载台区预测数量", blank=True, null=True)
    over_load_count_f = models.IntegerField(verbose_name="过载台区预测数量", blank=True, null=True)
    over_load_0_count_f = models.IntegerField(verbose_name="轻度过载台区预测数量", blank=True, null=True)
    over_load_1_count_f = models.IntegerField(verbose_name="中度过载台区预测数量", blank=True, null=True)
    over_load_2_count_f = models.IntegerField(verbose_name="重度过载台区预测数量", blank=True, null=True)
    over_load_3_count_f = models.IntegerField(verbose_name="异常过载台区预测数量", blank=True, null=True)

    load_average = models.DecimalField(verbose_name="平均负荷实际值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_average_f = models.DecimalField(verbose_name="平均负荷预测值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_max = models.DecimalField(verbose_name="最大负荷实际值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_max_f = models.DecimalField(verbose_name="最大负荷预测值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_min = models.DecimalField(verbose_name="最小负荷实际值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_min_f = models.DecimalField(verbose_name="最小负荷预测值", blank=True, null=True, max_digits=10, decimal_places=2)

    load_rate_average = models.DecimalField(verbose_name="平均负荷率", blank=True, null=True, max_digits=8, decimal_places=2)
    load_rate_min_average = models.DecimalField(verbose_name="平均最小负荷率", blank=True, null=True, max_digits=8, decimal_places=2)

    load_rate_average_f = models.DecimalField(verbose_name="平均负荷率预测值", blank=True, null=True, max_digits=8, decimal_places=2)
    load_rate_min_average_f = models.DecimalField(verbose_name="平均最小负荷率预测值", blank=True, null=True, max_digits=8,
                                                decimal_places=2)

    MOM_energy = models.DecimalField(verbose_name="用电量月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    MOM_energy_f = models.DecimalField(verbose_name="用电量预测值月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_energy = models.DecimalField(verbose_name="用电量年同比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_energy_f = models.DecimalField(verbose_name="用电量预测值年同比", blank=True, null=True, max_digits=8, decimal_places=2)

    MOM_load_average = models.DecimalField(verbose_name="平均负荷月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    MOM_load_average_f = models.DecimalField(verbose_name="平均负荷预测值月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_average = models.DecimalField(verbose_name="平均负荷年同比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_average_f = models.DecimalField(verbose_name="平均负荷预测值年同比", blank=True, null=True, max_digits=8, decimal_places=2)

    MOM_load_max = models.DecimalField(verbose_name="最大负荷月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    MOM_load_max_f = models.DecimalField(verbose_name="最大负荷预测值月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_max = models.DecimalField(verbose_name="最大负荷年同比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_max_f = models.DecimalField(verbose_name="最大负荷预测值年同比", blank=True, null=True, max_digits=8, decimal_places=2)

    MOM_load_min = models.DecimalField(verbose_name="最小负荷月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    MOM_load_min_f = models.DecimalField(verbose_name="最小负荷预测值月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_min = models.DecimalField(verbose_name="最小负荷年同比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_min_f = models.DecimalField(verbose_name="最小负荷预测值年同比", blank=True, null=True, max_digits=8, decimal_places=2)

    class Meta:
        db_table = 'sys_county_monthly'
        unique_together = ["month", "county_number"]
        verbose_name = "区县月数据表"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class StationLoadMonthly(models.Model):
    """供电服务所（站）月数据"""
    station_number = models.IntegerField(verbose_name="供电服务所（站）编号")
    county_number = models.IntegerField(verbose_name="所属区县编号")

    month = models.CharField(verbose_name="月份", max_length=255)
    normal_load_count = models.IntegerField(verbose_name="正常台区数量", blank=True, null=True)
    heavy_load_count = models.IntegerField(verbose_name="重载台区数量", blank=True, null=True)
    over_load_count = models.IntegerField(verbose_name="过载台区数量", blank=True, null=True)
    over_load_0_count = models.IntegerField(verbose_name="轻度过载台区数量", blank=True, null=True)
    over_load_1_count = models.IntegerField(verbose_name="中度过载台区数量", blank=True, null=True)
    over_load_2_count = models.IntegerField(verbose_name="重度过载台区数量", blank=True, null=True)
    over_load_3_count = models.IntegerField(verbose_name="异常过载台区数量", blank=True, null=True)

    normal_load_count_f = models.IntegerField(verbose_name="正常台区预测数量", blank=True, null=True)
    heavy_load_count_f = models.IntegerField(verbose_name="重载台区预测数量", blank=True, null=True)
    over_load_count_f = models.IntegerField(verbose_name="过载台区预测数量", blank=True, null=True)
    over_load_0_count_f = models.IntegerField(verbose_name="轻度过载台区预测数量", blank=True, null=True)
    over_load_1_count_f = models.IntegerField(verbose_name="中度过载台区预测数量", blank=True, null=True)
    over_load_2_count_f = models.IntegerField(verbose_name="重度过载台区预测数量", blank=True, null=True)
    over_load_3_count_f = models.IntegerField(verbose_name="异常过载台区预测数量", blank=True, null=True)

    load_average = models.DecimalField(verbose_name="平均负荷实际值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_average_f = models.DecimalField(verbose_name="平均负荷预测值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_max = models.DecimalField(verbose_name="最大负荷实际值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_max_f = models.DecimalField(verbose_name="最大负荷预测值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_min = models.DecimalField(verbose_name="最小负荷实际值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_min_f = models.DecimalField(verbose_name="最小负荷预测值", blank=True, null=True, max_digits=10, decimal_places=2)

    load_rate_average = models.DecimalField(verbose_name="平均负荷率", blank=True, null=True, max_digits=8, decimal_places=2)
    load_rate_min_average = models.DecimalField(verbose_name="平均最小负荷率", blank=True, null=True, max_digits=8, decimal_places=2)

    load_rate_average_f = models.DecimalField(verbose_name="平均负荷率预测值", blank=True, null=True, max_digits=8, decimal_places=2)
    load_rate_min_average_f = models.DecimalField(verbose_name="平均最小负荷率预测值", blank=True, null=True, max_digits=8,
                                                decimal_places=2)

    MOM_energy = models.DecimalField(verbose_name="用电量月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    MOM_energy_f = models.DecimalField(verbose_name="用电量预测值月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_energy = models.DecimalField(verbose_name="用电量年同比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_energy_f = models.DecimalField(verbose_name="用电量预测值年同比", blank=True, null=True, max_digits=8, decimal_places=2)

    MOM_load_average = models.DecimalField(verbose_name="平均负荷月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    MOM_load_average_f = models.DecimalField(verbose_name="平均负荷预测值月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_average = models.DecimalField(verbose_name="平均负荷年同比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_average_f = models.DecimalField(verbose_name="平均负荷预测值年同比", blank=True, null=True, max_digits=8, decimal_places=2)

    MOM_load_max = models.DecimalField(verbose_name="最大负荷月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    MOM_load_max_f = models.DecimalField(verbose_name="最大负荷预测值月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_max = models.DecimalField(verbose_name="最大负荷年同比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_max_f = models.DecimalField(verbose_name="最大负荷预测值年同比", blank=True, null=True, max_digits=8, decimal_places=2)

    MOM_load_min = models.DecimalField(verbose_name="最小负荷月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    MOM_load_min_f = models.DecimalField(verbose_name="最小负荷预测值月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_min = models.DecimalField(verbose_name="最小负荷年同比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_min_f = models.DecimalField(verbose_name="最小负荷预测值年同比", blank=True, null=True, max_digits=8, decimal_places=2)

    class Meta:
        db_table = 'sys_station_monthly'
        unique_together = ["month", "station_number"]
        verbose_name = "供电服务所（站）月数据表"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class ManagerLoadMonthly(models.Model):
    """台区经理月数据"""
    manager_number = models.IntegerField(verbose_name="台区经理编号")
    station_number = models.IntegerField(verbose_name="所属供电服务所（站）编号")
    county_number = models.IntegerField(verbose_name="所属区县编号")

    month = models.CharField(verbose_name="月份", max_length=255)
    normal_load_count = models.IntegerField(verbose_name="正常台区数量", blank=True, null=True)
    heavy_load_count = models.IntegerField(verbose_name="重载台区数量", blank=True, null=True)
    over_load_count = models.IntegerField(verbose_name="过载台区数量", blank=True, null=True)
    over_load_0_count = models.IntegerField(verbose_name="轻度过载台区数量", blank=True, null=True)
    over_load_1_count = models.IntegerField(verbose_name="中度过载台区数量", blank=True, null=True)
    over_load_2_count = models.IntegerField(verbose_name="重度过载台区数量", blank=True, null=True)
    over_load_3_count = models.IntegerField(verbose_name="异常过载台区数量", blank=True, null=True)

    normal_load_count_f = models.IntegerField(verbose_name="正常台区预测数量", blank=True, null=True)
    heavy_load_count_f = models.IntegerField(verbose_name="重载台区预测数量", blank=True, null=True)
    over_load_count_f = models.IntegerField(verbose_name="过载台区预测数量", blank=True, null=True)
    over_load_0_count_f = models.IntegerField(verbose_name="轻度过载台区预测数量", blank=True, null=True)
    over_load_1_count_f = models.IntegerField(verbose_name="中度过载台区预测数量", blank=True, null=True)
    over_load_2_count_f = models.IntegerField(verbose_name="重度过载台区预测数量", blank=True, null=True)
    over_load_3_count_f = models.IntegerField(verbose_name="异常过载台区预测数量", blank=True, null=True)

    # 以下月度指标取日数据的平均值
    load_average = models.DecimalField(verbose_name="平均负荷实际值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_average_f = models.DecimalField(verbose_name="平均负荷预测值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_max = models.DecimalField(verbose_name="最大负荷实际值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_max_f = models.DecimalField(verbose_name="最大负荷预测值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_min = models.DecimalField(verbose_name="最小负荷实际值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_min_f = models.DecimalField(verbose_name="最小负荷预测值", blank=True, null=True, max_digits=10, decimal_places=2)

    load_rate_average = models.DecimalField(verbose_name="平均负荷率", blank=True, null=True, max_digits=8, decimal_places=2)
    load_rate_min_average = models.DecimalField(verbose_name="平均最小负荷率", blank=True, null=True, max_digits=8,
                                                decimal_places=2)

    load_rate_average_f = models.DecimalField(verbose_name="平均负荷率预测值", blank=True, null=True, max_digits=8,
                                              decimal_places=2)
    load_rate_min_average_f = models.DecimalField(verbose_name="平均最小负荷率预测值", blank=True, null=True, max_digits=8,
                                                  decimal_places=2)

    MOM_energy = models.DecimalField(verbose_name="用电量月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    MOM_energy_f = models.DecimalField(verbose_name="用电量预测值月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_energy = models.DecimalField(verbose_name="用电量年同比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_energy_f = models.DecimalField(verbose_name="用电量预测值年同比", blank=True, null=True, max_digits=8, decimal_places=2)

    MOM_load_average = models.DecimalField(verbose_name="平均负荷月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    MOM_load_average_f = models.DecimalField(verbose_name="平均负荷预测值月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_average = models.DecimalField(verbose_name="平均负荷年同比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_average_f = models.DecimalField(verbose_name="平均负荷预测值年同比", blank=True, null=True, max_digits=8, decimal_places=2)

    MOM_load_max = models.DecimalField(verbose_name="最大负荷月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    MOM_load_max_f = models.DecimalField(verbose_name="最大负荷预测值月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_max = models.DecimalField(verbose_name="最大负荷年同比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_max_f = models.DecimalField(verbose_name="最大负荷预测值年同比", blank=True, null=True, max_digits=8, decimal_places=2)

    MOM_load_min = models.DecimalField(verbose_name="最小负荷月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    MOM_load_min_f = models.DecimalField(verbose_name="最小负荷预测值月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_min = models.DecimalField(verbose_name="最小负荷年同比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_min_f = models.DecimalField(verbose_name="最小负荷预测值年同比", blank=True, null=True, max_digits=8, decimal_places=2)

    class Meta:
        db_table = 'sys_manager_monthly'
        unique_together = ["month", "manager_number"]
        verbose_name = "台区经理月数据表"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class TransformerLoadMonthlyBase(models.Model):
    """台区月负荷"""
    name = models.CharField(verbose_name="台区名", max_length=255, default="某台区")
    transformer_number = models.IntegerField(verbose_name="台区编号")
    manager_number = models.IntegerField(verbose_name="所属台区经理编号", blank=True, null=True)
    station_number = models.IntegerField(verbose_name="所属供电服务所（站）编号")
    county_number = models.IntegerField(verbose_name="所属区县编号")

    volume = models.IntegerField(verbose_name="台区额定容量")
    multiplying_power = models.IntegerField(verbose_name="台区倍率")

    month = models.CharField(verbose_name="月份", max_length=255, default='')

    energy_consumption = models.DecimalField(verbose_name="月用电量", blank=True, null=True, max_digits=10, decimal_places=2)
    energy_consumption_manual = models.DecimalField(verbose_name="月用电量人工修正值", blank=True, null=True, max_digits=10, decimal_places=2)
    energy_consumption_manual_status = models.SmallIntegerField(verbose_name="月用电量是否人工修正", default=0, blank=True)
    energy_consumption_f = models.DecimalField(verbose_name="月用电量综合预测值", blank=True, null=True, max_digits=10, decimal_places=2)
    energy_consumption_f_1 = models.DecimalField(verbose_name="月用电量推荐预测值_算法1", blank=True, null=True, max_digits=8, decimal_places=2)
    energy_consumption_f_2 = models.DecimalField(verbose_name="月用电量综合预测值_算法2", blank=True, null=True, max_digits=8, decimal_places=2)
    energy_consumption_f_3 = models.DecimalField(verbose_name="月用电量综合预测值_算法3", blank=True, null=True, max_digits=8, decimal_places=2)
    energy_consumption_f_4 = models.DecimalField(verbose_name="月用电量综合预测值_算法4", blank=True, null=True, max_digits=8, decimal_places=2)
    energy_consumption_f_5 = models.DecimalField(verbose_name="月用电量综合预测值_算法5", blank=True, null=True, max_digits=8, decimal_places=2)

    normal_load_count = models.IntegerField(verbose_name="正常次数", blank=True, null=True)
    heavy_load_count = models.IntegerField(verbose_name="重载次数", blank=True, null=True)
    over_load_count = models.IntegerField(verbose_name="过载次数", blank=True, null=True)
    over_load_0_count = models.IntegerField(verbose_name="轻度过载次数", blank=True, null=True)
    over_load_1_count = models.IntegerField(verbose_name="中度过载次数", blank=True, null=True)
    over_load_2_count = models.IntegerField(verbose_name="重度过载次数", blank=True, null=True)
    over_load_3_count = models.IntegerField(verbose_name="异常过载次数", blank=True, null=True)
    load_status = models.CharField(verbose_name="最常见的重过载状态", max_length=255, default="正常")

    normal_load_count_f = models.IntegerField(verbose_name="正常预测准确次数", blank=True, null=True)
    heavy_load_count_f = models.IntegerField(verbose_name="重载预测准确次数", blank=True, null=True)
    over_load_count_f = models.IntegerField(verbose_name="过载预测准确次数", blank=True, null=True)
    over_load_0_count_f = models.IntegerField(verbose_name="轻度过载预测准确次数", blank=True, null=True)
    over_load_1_count_f = models.IntegerField(verbose_name="中度过载预测准确次数", blank=True, null=True)
    over_load_2_count_f = models.IntegerField(verbose_name="重度过载预测准确次数", blank=True, null=True)
    over_load_3_count_f = models.IntegerField(verbose_name="异常过载预测准确次数", blank=True, null=True)

    # 平均负荷月度指标
    load_average = models.DecimalField(verbose_name="月平均负荷实际值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_average_f = models.DecimalField(verbose_name="月平均负荷预测值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_average_min = models.DecimalField(verbose_name="本月日均负荷最小值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_average_f_min = models.DecimalField(verbose_name="本月日均负荷预测值最小值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_average_max = models.DecimalField(verbose_name="本月日均负荷最大值", blank=True, null=True, max_digits=10,
                                           decimal_places=2)
    load_average_f_max = models.DecimalField(verbose_name="本月日均负荷预测值最大值", blank=True, null=True, max_digits=10,
                                             decimal_places=2)
    # 最大负荷月度指标
    load_max = models.DecimalField(verbose_name="本月日峰荷实际值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_max_f = models.DecimalField(verbose_name="本月日峰荷预测值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_max_min = models.DecimalField(verbose_name="本月日峰荷最小值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_max_f_min = models.DecimalField(verbose_name="本月日峰荷预测值最小值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_max_max = models.DecimalField(verbose_name="本月最高负荷", blank=True, null=True, max_digits=10, decimal_places=2)
    load_max_f_max = models.DecimalField(verbose_name="本月最高负荷预测值", blank=True, null=True, max_digits=10,
                                         decimal_places=2)
    # 最小负荷月度指标
    load_min = models.DecimalField(verbose_name="本月日谷荷实际值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_min_f = models.DecimalField(verbose_name="本月日谷荷预测值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_min_min = models.DecimalField(verbose_name="本月最低负荷", blank=True, null=True, max_digits=10, decimal_places=2)
    load_min_f_min = models.DecimalField(verbose_name="本月最低负荷预测值", blank=True, null=True, max_digits=10,
                                         decimal_places=2)
    load_min_max = models.DecimalField(verbose_name="本月日谷荷最大值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_min_f_max = models.DecimalField(verbose_name="本月日谷荷预测值最大值", blank=True, null=True, max_digits=10,
                                         decimal_places=2)
    # 负荷率
    load_rate = models.DecimalField(verbose_name="本月日负荷率平均值", blank=True, null=True, max_digits=8, decimal_places=2)
    load_rate_f = models.DecimalField(verbose_name="本月日负荷率预测值平均值", blank=True, null=True, max_digits=8,
                                            decimal_places=2)
    load_rate_min = models.DecimalField(verbose_name="本月日负荷率最小值", blank=True, null=True, max_digits=8,
                                            decimal_places=2)
    load_rate_f_min = models.DecimalField(verbose_name="本月日负荷率预测值最小值", blank=True, null=True, max_digits=8,
                                              decimal_places=2)
    load_rate_max = models.DecimalField(verbose_name="本月日负荷率最大值", blank=True, null=True, max_digits=8,
                                            decimal_places=2)
    load_rate_f_max = models.DecimalField(verbose_name="本月日负荷率预测值最大值", blank=True, null=True, max_digits=8,
                                              decimal_places=2)

    # 最小负荷率
    load_min_rate = models.DecimalField(verbose_name="本月日最小负荷率平均值", blank=True, null=True, max_digits=8,
                                                decimal_places=2)
    load_min_rate_f = models.DecimalField(verbose_name="本月日最小负荷率预测值平均值", blank=True, null=True, max_digits=8,
                                                  decimal_places=2)
    load_min_rate_min = models.DecimalField(verbose_name="本月日最小负荷率最小值", blank=True, null=True, max_digits=8,
                                        decimal_places=2)
    load_min_rate_f_min = models.DecimalField(verbose_name="本月日最小负荷率预测值最小值", blank=True, null=True, max_digits=8,
                                          decimal_places=2)
    load_min_rate_max = models.DecimalField(verbose_name="本月日最小负荷率最大值", blank=True, null=True, max_digits=8,
                                        decimal_places=2)
    load_min_rate_f_max = models.DecimalField(verbose_name="本月日最小负荷率预测值最大值", blank=True, null=True, max_digits=8,
                                          decimal_places=2)
    # added on 1230
    load_f_accuracy = models.DecimalField(verbose_name="本月累计负荷预测精度", blank=True, null=True, max_digits=8, decimal_places=2)

    # added on 1107
    peak_valley_difference = models.DecimalField(verbose_name="本月日谷峰差平均值", blank=True, null=True, max_digits=8,
                                                 decimal_places=2)
    peak_valley_difference_f = models.DecimalField(verbose_name="本月日谷峰差预测值平均值", blank=True, null=True, max_digits=8,
                                                   decimal_places=2)
    peak_valley_difference_min = models.DecimalField(verbose_name="本月日谷峰差最小值", blank=True, null=True, max_digits=8,
                                                 decimal_places=2)
    peak_valley_difference_f_min = models.DecimalField(verbose_name="本月日谷峰差预测值最小值", blank=True, null=True, max_digits=8,
                                                   decimal_places=2)
    peak_valley_difference_max = models.DecimalField(verbose_name="本月日谷峰差最大值", blank=True, null=True, max_digits=8,
                                                     decimal_places=2)
    peak_valley_difference_f_max = models.DecimalField(verbose_name="本月日谷峰差预测值最大值", blank=True, null=True, max_digits=8,
                                                       decimal_places=2)
    # -----------------------------
    MOM_energy = models.DecimalField(verbose_name="用电量月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    MOM_energy_f = models.DecimalField(verbose_name="用电量预测值月环比", blank=True, null=True, max_digits=8, decimal_places=2)

    YOY_energy = models.DecimalField(verbose_name="用电量年同比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_energy_f = models.DecimalField(verbose_name="用电量预测值年同比", blank=True, null=True, max_digits=8, decimal_places=2)

    MOM_load_average = models.DecimalField(verbose_name="平均负荷月环比", blank=True, null=True, max_digits=8,
                                           decimal_places=2)
    MOM_load_average_f = models.DecimalField(verbose_name="平均负荷预测值月环比", blank=True, null=True, max_digits=8,
                                             decimal_places=2)
    YOY_load_average = models.DecimalField(verbose_name="平均负荷年同比", blank=True, null=True, max_digits=8,
                                           decimal_places=2)
    YOY_load_average_f = models.DecimalField(verbose_name="平均负荷预测值年同比", blank=True, null=True, max_digits=8,
                                             decimal_places=2)

    MOM_load_max = models.DecimalField(verbose_name="最大负荷月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    MOM_load_max_f = models.DecimalField(verbose_name="最大负荷预测值月环比", blank=True, null=True, max_digits=8,
                                         decimal_places=2)
    YOY_load_max = models.DecimalField(verbose_name="最大负荷年同比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_max_f = models.DecimalField(verbose_name="最大负荷预测值年同比", blank=True, null=True, max_digits=8,
                                         decimal_places=2)

    MOM_load_min = models.DecimalField(verbose_name="最小负荷月环比", blank=True, null=True, max_digits=8, decimal_places=2)
    MOM_load_min_f = models.DecimalField(verbose_name="最小负荷预测值月环比", blank=True, null=True, max_digits=8,
                                         decimal_places=2)
    YOY_load_min = models.DecimalField(verbose_name="最小负荷年同比", blank=True, null=True, max_digits=8, decimal_places=2)
    YOY_load_min_f = models.DecimalField(verbose_name="最小负荷预测值年同比", blank=True, null=True, max_digits=8,
                                         decimal_places=2)

    class Meta:
        db_table = 'sys_transformer_monthly'
        unique_together = ["month", "transformer_number"]
        verbose_name = "台区月数据表"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class ProvinceLoadDaily(models.Model):
    """省日数据"""
    province_number = models.IntegerField(verbose_name="省编号")

    time = models.DateTimeField(verbose_name="时间")
    day_of_week = models.SmallIntegerField(verbose_name="星期", blank=True, null=True)
    is_working_day = models.SmallIntegerField(verbose_name="是否工作日", blank=True, null=True)

    normal_load_count = models.IntegerField(verbose_name="正常台区数量", blank=True, null=True)
    heavy_load_count = models.IntegerField(verbose_name="重载台区数量", blank=True, null=True)
    over_load_count = models.IntegerField(verbose_name="过载台区数量", blank=True, null=True)
    over_load_0_count = models.IntegerField(verbose_name="轻度过载台区数量", blank=True, null=True)
    over_load_1_count = models.IntegerField(verbose_name="中度过载台区数量", blank=True, null=True)
    over_load_2_count = models.IntegerField(verbose_name="重度过载台区数量", blank=True, null=True)
    over_load_3_count = models.IntegerField(verbose_name="异常过载台区数量", blank=True, null=True)

    normal_load_count_f = models.IntegerField(verbose_name="正常台区预测数量", blank=True, null=True)
    heavy_load_count_f = models.IntegerField(verbose_name="重载台区预测数量", blank=True, null=True)
    over_load_count_f = models.IntegerField(verbose_name="过载台区预测数量", blank=True, null=True)
    over_load_0_count_f = models.IntegerField(verbose_name="轻度过载台区预测数量", blank=True, null=True)
    over_load_1_count_f = models.IntegerField(verbose_name="中度过载台区预测数量", blank=True, null=True)
    over_load_2_count_f = models.IntegerField(verbose_name="重度过载台区预测数量", blank=True, null=True)
    over_load_3_count_f = models.IntegerField(verbose_name="异常过载台区预测数量", blank=True, null=True)

    load_average = models.DecimalField(verbose_name="平均负荷实际值", blank=True, null=True, max_digits=10,
                                               decimal_places=2)
    load_average_f = models.DecimalField(verbose_name="平均负荷预测值", blank=True, null=True, max_digits=10,
                                                 decimal_places=2)
    load_max = models.DecimalField(verbose_name="最大负荷实际值", blank=True, null=True, max_digits=10,
                                           decimal_places=2)
    load_max_f = models.DecimalField(verbose_name="最大负荷预测值", blank=True, null=True, max_digits=10,
                                             decimal_places=2)
    load_min = models.DecimalField(verbose_name="最小负荷实际值", blank=True, null=True, max_digits=10,
                                           decimal_places=2)
    load_min_f = models.DecimalField(verbose_name="最小负荷预测值", blank=True, null=True, max_digits=10,
                                             decimal_places=2)

    load_rate_average = models.DecimalField(verbose_name="平均负荷率", blank=True, null=True, max_digits=8, decimal_places=2)
    load_rate_min_average = models.DecimalField(verbose_name="平均最小负荷率", blank=True, null=True, max_digits=8, decimal_places=2)

    load_rate_average_f = models.DecimalField(verbose_name="平均负荷率预测值", blank=True, null=True, max_digits=8, decimal_places=2)
    load_rate_min_average_f = models.DecimalField(verbose_name="平均最小负荷率预测值", blank=True, null=True, max_digits=8,
                                                decimal_places=2)

    class Meta:
        db_table = 'sys_province_daily'
        unique_together = ["time", "province_number"]
        verbose_name = "省日数据表"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class CityLoadDaily(models.Model):
    """城市日数据"""
    city_number = models.IntegerField(verbose_name="城市编号")
    province_number = models.IntegerField(verbose_name="所属省编号")

    time = models.DateTimeField(verbose_name="时间")
    day_of_week = models.SmallIntegerField(verbose_name="星期", blank=True, null=True)
    is_working_day = models.SmallIntegerField(verbose_name="是否工作日", blank=True, null=True)

    normal_load_count = models.IntegerField(verbose_name="正常台区数量", blank=True, null=True)
    heavy_load_count = models.IntegerField(verbose_name="重载台区数量", blank=True, null=True)
    over_load_count = models.IntegerField(verbose_name="过载台区数量", blank=True, null=True)
    over_load_0_count = models.IntegerField(verbose_name="轻度过载台区数量", blank=True, null=True)
    over_load_1_count = models.IntegerField(verbose_name="中度过载台区数量", blank=True, null=True)
    over_load_2_count = models.IntegerField(verbose_name="重度过载台区数量", blank=True, null=True)
    over_load_3_count = models.IntegerField(verbose_name="异常过载台区数量", blank=True, null=True)

    normal_load_count_f = models.IntegerField(verbose_name="正常台区预测数量", blank=True, null=True)
    heavy_load_count_f = models.IntegerField(verbose_name="重载台区预测数量", blank=True, null=True)
    over_load_count_f = models.IntegerField(verbose_name="过载台区预测数量", blank=True, null=True)
    over_load_0_count_f = models.IntegerField(verbose_name="轻度过载台区预测数量", blank=True, null=True)
    over_load_1_count_f = models.IntegerField(verbose_name="中度过载台区预测数量", blank=True, null=True)
    over_load_2_count_f = models.IntegerField(verbose_name="重度过载台区预测数量", blank=True, null=True)
    over_load_3_count_f = models.IntegerField(verbose_name="异常过载台区预测数量", blank=True, null=True)

    load_average = models.DecimalField(verbose_name="平均负荷实际值", blank=True, null=True, max_digits=10,
                                               decimal_places=2)
    load_average_f = models.DecimalField(verbose_name="平均负荷预测值", blank=True, null=True, max_digits=10,
                                                 decimal_places=2)
    load_max = models.DecimalField(verbose_name="最大负荷实际值", blank=True, null=True, max_digits=10,
                                           decimal_places=2)
    load_max_f = models.DecimalField(verbose_name="最大负荷预测值", blank=True, null=True, max_digits=10,
                                             decimal_places=2)
    load_min = models.DecimalField(verbose_name="最小负荷实际值", blank=True, null=True, max_digits=10,
                                           decimal_places=2)
    load_min_f = models.DecimalField(verbose_name="最小负荷预测值", blank=True, null=True, max_digits=10,
                                             decimal_places=2)

    load_rate_average = models.DecimalField(verbose_name="平均负荷率", blank=True, null=True, max_digits=8, decimal_places=2)
    load_rate_min_average = models.DecimalField(verbose_name="平均最小负荷率", blank=True, null=True, max_digits=8, decimal_places=2)

    load_rate_average_f = models.DecimalField(verbose_name="平均负荷率预测值", blank=True, null=True, max_digits=8, decimal_places=2)
    load_rate_min_average_f = models.DecimalField(verbose_name="平均最小负荷率预测值", blank=True, null=True, max_digits=8,
                                                decimal_places=2)

    class Meta:
        db_table = 'sys_city_daily'
        unique_together = ["time", "city_number"]
        verbose_name = "城市日数据表"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class CountyLoadDaily(models.Model):
    """区县日数据"""
    county_number = models.IntegerField(verbose_name="区县编号")
    city_number = models.IntegerField(verbose_name="所属城市编号")

    time = models.DateTimeField(verbose_name="时间")
    day_of_week = models.SmallIntegerField(verbose_name="星期", blank=True, null=True)
    is_working_day = models.SmallIntegerField(verbose_name="是否工作日", blank=True, null=True)

    normal_load_count = models.IntegerField(verbose_name="正常台区数量", blank=True, null=True)
    heavy_load_count = models.IntegerField(verbose_name="重载台区数量", blank=True, null=True)
    over_load_count = models.IntegerField(verbose_name="过载台区数量", blank=True, null=True)
    over_load_0_count = models.IntegerField(verbose_name="轻度过载台区数量", blank=True, null=True)
    over_load_1_count = models.IntegerField(verbose_name="中度过载台区数量", blank=True, null=True)
    over_load_2_count = models.IntegerField(verbose_name="重度过载台区数量", blank=True, null=True)
    over_load_3_count = models.IntegerField(verbose_name="异常过载台区数量", blank=True, null=True)

    normal_load_count_f = models.IntegerField(verbose_name="正常台区预测数量", blank=True, null=True)
    heavy_load_count_f = models.IntegerField(verbose_name="重载台区预测数量", blank=True, null=True)
    over_load_count_f = models.IntegerField(verbose_name="过载台区预测数量", blank=True, null=True)
    over_load_0_count_f = models.IntegerField(verbose_name="轻度过载台区预测数量", blank=True, null=True)
    over_load_1_count_f = models.IntegerField(verbose_name="中度过载台区预测数量", blank=True, null=True)
    over_load_2_count_f = models.IntegerField(verbose_name="重度过载台区预测数量", blank=True, null=True)
    over_load_3_count_f = models.IntegerField(verbose_name="异常过载台区预测数量", blank=True, null=True)

    load_average = models.DecimalField(verbose_name="平均负荷实际值", blank=True, null=True, max_digits=10,
                                               decimal_places=2)
    load_average_f = models.DecimalField(verbose_name="平均负荷预测值", blank=True, null=True, max_digits=10,
                                                 decimal_places=2)
    load_max = models.DecimalField(verbose_name="最大负荷实际值", blank=True, null=True, max_digits=10,
                                           decimal_places=2)
    load_max_f = models.DecimalField(verbose_name="最大负荷预测值", blank=True, null=True, max_digits=10,
                                             decimal_places=2)
    load_min = models.DecimalField(verbose_name="最小负荷实际值", blank=True, null=True, max_digits=10,
                                           decimal_places=2)
    load_min_f = models.DecimalField(verbose_name="最小负荷预测值", blank=True, null=True, max_digits=10,
                                             decimal_places=2)

    load_rate_average = models.DecimalField(verbose_name="平均负荷率", blank=True, null=True, max_digits=8, decimal_places=2)
    load_rate_min_average = models.DecimalField(verbose_name="平均最小负荷率", blank=True, null=True, max_digits=8, decimal_places=2)

    load_rate_average_f = models.DecimalField(verbose_name="平均负荷率预测值", blank=True, null=True, max_digits=8, decimal_places=2)
    load_rate_min_average_f = models.DecimalField(verbose_name="平均最小负荷率预测值", blank=True, null=True, max_digits=8,
                                                decimal_places=2)

    class Meta:
        db_table = 'sys_county_daily'
        unique_together = ["time", "county_number"]
        verbose_name = "区县日数据表"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class StationLoadDaily(models.Model):
    """供电服务所（站）日数据"""
    station_number = models.IntegerField(verbose_name="供电服务所（站）编号")
    county_number = models.IntegerField(verbose_name="所属区县编号")

    time = models.DateTimeField(verbose_name="时间")
    day_of_week = models.SmallIntegerField(verbose_name="星期", blank=True, null=True)
    is_working_day = models.SmallIntegerField(verbose_name="是否工作日", blank=True, null=True)

    normal_load_count = models.IntegerField(verbose_name="正常台区数量", blank=True, null=True)
    heavy_load_count = models.IntegerField(verbose_name="重载台区数量", blank=True, null=True)
    over_load_count = models.IntegerField(verbose_name="过载台区数量", blank=True, null=True)
    over_load_0_count = models.IntegerField(verbose_name="轻度过载台区数量", blank=True, null=True)
    over_load_1_count = models.IntegerField(verbose_name="中度过载台区数量", blank=True, null=True)
    over_load_2_count = models.IntegerField(verbose_name="重度过载台区数量", blank=True, null=True)
    over_load_3_count = models.IntegerField(verbose_name="异常过载台区数量", blank=True, null=True)

    normal_load_count_f = models.IntegerField(verbose_name="正常台区预测数量", blank=True, null=True)
    heavy_load_count_f = models.IntegerField(verbose_name="重载台区预测数量", blank=True, null=True)
    over_load_count_f = models.IntegerField(verbose_name="过载台区预测数量", blank=True, null=True)
    over_load_0_count_f = models.IntegerField(verbose_name="轻度过载台区预测数量", blank=True, null=True)
    over_load_1_count_f = models.IntegerField(verbose_name="中度过载台区预测数量", blank=True, null=True)
    over_load_2_count_f = models.IntegerField(verbose_name="重度过载台区预测数量", blank=True, null=True)
    over_load_3_count_f = models.IntegerField(verbose_name="异常过载台区预测数量", blank=True, null=True)

    load_average = models.DecimalField(verbose_name="平均负荷实际值", blank=True, null=True, max_digits=10,
                                               decimal_places=2)
    load_average_f = models.DecimalField(verbose_name="平均负荷预测值", blank=True, null=True, max_digits=10,
                                                 decimal_places=2)
    load_max = models.DecimalField(verbose_name="最大负荷实际值", blank=True, null=True, max_digits=10,
                                                decimal_places=2)
    load_max_f = models.DecimalField(verbose_name="最大负荷预测值", blank=True, null=True, max_digits=10,
                                             decimal_places=2)
    load_min = models.DecimalField(verbose_name="最小负荷实际值", blank=True, null=True, max_digits=10,
                                           decimal_places=2)
    load_min_f = models.DecimalField(verbose_name="最小负荷预测值", blank=True, null=True, max_digits=10,
                                             decimal_places=2)

    load_rate_average = models.DecimalField(verbose_name="平均负荷率", blank=True, null=True, max_digits=8, decimal_places=2)
    load_rate_min_average = models.DecimalField(verbose_name="平均最小负荷率", blank=True, null=True, max_digits=8, decimal_places=2)

    load_rate_average_f = models.DecimalField(verbose_name="平均负荷率预测值", blank=True, null=True, max_digits=8, decimal_places=2)
    load_rate_min_average_f = models.DecimalField(verbose_name="平均最小负荷率预测值", blank=True, null=True, max_digits=8,
                                                decimal_places=2)

    class Meta:
        db_table = 'sys_station_daily'
        unique_together = ["time", "station_number"]
        verbose_name = "供电服务所（站）日数据表"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class ManagerLoadDaily(models.Model):
    """台区经理日数据"""
    manager_number = models.IntegerField(verbose_name="台区经理编号")
    station_number = models.IntegerField(verbose_name="所属供电服务所（站）编号")
    county_number = models.IntegerField(verbose_name="所属区县编号")

    # volume = models.IntegerField(verbose_name="台区额定容量")
    # multiplying_power = models.IntegerField(verbose_name="台区倍率")
    time = models.DateTimeField(verbose_name="时间")
    day_of_week = models.SmallIntegerField(verbose_name="星期", blank=True, null=True)
    is_working_day = models.SmallIntegerField(verbose_name="是否工作日", blank=True, null=True)

    normal_load_count = models.IntegerField(verbose_name="正常台区数量", blank=True, null=True)
    heavy_load_count = models.IntegerField(verbose_name="重载台区数量", blank=True, null=True)
    over_load_count = models.IntegerField(verbose_name="过载台区数量", blank=True, null=True)
    over_load_0_count = models.IntegerField(verbose_name="轻度过载台区数量", blank=True, null=True)
    over_load_1_count = models.IntegerField(verbose_name="中度过载台区数量", blank=True, null=True)
    over_load_2_count = models.IntegerField(verbose_name="重度过载台区数量", blank=True, null=True)
    over_load_3_count = models.IntegerField(verbose_name="异常过载台区数量", blank=True, null=True)

    normal_load_count_f = models.IntegerField(verbose_name="正常台区预测数量", blank=True, null=True)
    heavy_load_count_f = models.IntegerField(verbose_name="重载台区预测数量", blank=True, null=True)
    over_load_count_f = models.IntegerField(verbose_name="过载台区预测数量", blank=True, null=True)
    over_load_0_count_f = models.IntegerField(verbose_name="轻度过载台区预测数量", blank=True, null=True)
    over_load_1_count_f = models.IntegerField(verbose_name="中度过载台区预测数量", blank=True, null=True)
    over_load_2_count_f = models.IntegerField(verbose_name="重度过载台区预测数量", blank=True, null=True)
    over_load_3_count_f = models.IntegerField(verbose_name="异常过载台区预测数量", blank=True, null=True)

    load_average = models.DecimalField(verbose_name="平均负荷实际值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_average_f = models.DecimalField(verbose_name="平均负荷预测值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_max = models.DecimalField(verbose_name="最大负荷实际值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_max_f = models.DecimalField(verbose_name="最大负荷预测值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_min = models.DecimalField(verbose_name="最小负荷实际值", blank=True, null=True, max_digits=10, decimal_places=2)
    load_min_f = models.DecimalField(verbose_name="最小负荷预测值", blank=True, null=True, max_digits=10, decimal_places=2)

    load_rate_average = models.DecimalField(verbose_name="平均负荷率", blank=True, null=True, max_digits=8, decimal_places=2)
    load_rate_min_average = models.DecimalField(verbose_name="平均最小负荷率", blank=True, null=True, max_digits=8, decimal_places=2)

    load_rate_average_f = models.DecimalField(verbose_name="平均负荷率预测值", blank=True, null=True, max_digits=8, decimal_places=2)
    load_rate_min_average_f = models.DecimalField(verbose_name="平均最小负荷率预测值", blank=True, null=True, max_digits=8,
                                                decimal_places=2)

    class Meta:
        db_table = 'sys_manager_daily'
        unique_together = ["time", "manager_number"]
        verbose_name = "台区经理日数据表"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class TransformerLoadDailyBase(models.Model):
    """台区日负荷"""
    name = models.CharField(verbose_name="台区名", max_length=255, default="某台区")
    transformer_number = models.IntegerField(verbose_name="台区编号")
    manager_number = models.IntegerField(verbose_name="所属台区经理编号", blank=True, null=True)
    station_number = models.IntegerField(verbose_name="所属供电服务所（站）编号")
    county_number = models.IntegerField(verbose_name="所属区县编号")

    # volume = models.IntegerField(verbose_name="台区额定容量")
    # multiplying_power = models.IntegerField(verbose_name="台区倍率")

    time = models.DateTimeField(verbose_name="时间")
    day_of_week = models.SmallIntegerField(verbose_name="星期", blank=True, null=True)
    is_working_day = models.SmallIntegerField(verbose_name="是否工作日", blank=True, null=True)
    energy_consumption = models.DecimalField(verbose_name="用电量", blank=True, null=True, max_digits=10, decimal_places=2)
    energy_consumption_manual = models.DecimalField(verbose_name="用电量人工修正值", blank=True, null=True, max_digits=8, decimal_places=2)
    energy_consumption_manual_status = models.SmallIntegerField(verbose_name="用电量是否人工修正", default=0, blank=True)
    energy_consumption_f = models.DecimalField(verbose_name="用电量综合预测值", blank=True, null=True, max_digits=10, decimal_places=2)
    energy_consumption_f_1 = models.DecimalField(verbose_name="用电量推荐预测值_算法1", blank=True, null=True, max_digits=8, decimal_places=2)
    energy_consumption_f_2 = models.DecimalField(verbose_name="用电量综合预测值_算法2", blank=True, null=True, max_digits=8, decimal_places=2)
    energy_consumption_f_3 = models.DecimalField(verbose_name="用电量综合预测值_算法3", blank=True, null=True, max_digits=8, decimal_places=2)
    energy_consumption_f_4 = models.DecimalField(verbose_name="用电量综合预测值_算法4", blank=True, null=True, max_digits=8, decimal_places=2)
    energy_consumption_f_5 = models.DecimalField(verbose_name="用电量综合预测值_算法5", blank=True, null=True, max_digits=8, decimal_places=2)

    WOW_energy = models.DecimalField(verbose_name="用电量周环比", blank=True, null=True, max_digits=5, decimal_places=2)
    WOW_energy_f = models.DecimalField(verbose_name="用电量预测值周环比", blank=True, null=True, max_digits=5, decimal_places=2)

    # power_slice_1 = models.DecimalField(verbose_name="第一个点负荷值")
    # 96 points of load value
    names = locals()

    for i in range(1, 97):
        names['power_slice_' + str(i)] = models.DecimalField(verbose_name="第" + str(i) + "个点负荷值", blank=True, null=True, max_digits=5, decimal_places=2)
        names['power_slice_' + str(i) + "_f"] = models.DecimalField(verbose_name="第" + str(i) + "个点负荷推荐预测值", blank=True, null=True, max_digits=5, decimal_places=2)
        names['power_slice_' + str(i) + "_f_1"] = models.DecimalField(verbose_name="第" + str(i) + "个点负荷预测值_算法1", blank=True, null=True, max_digits=5, decimal_places=2)
        names['power_slice_' + str(i) + "_f_2"] = models.DecimalField(verbose_name="第" + str(i) + "个点负荷预测值_算法2", blank=True, null=True, max_digits=5, decimal_places=2)
        names['power_slice_' + str(i) + "_f_3"] = models.DecimalField(verbose_name="第" + str(i) + "个点负荷预测值_算法3", blank=True, null=True, max_digits=5, decimal_places=2)
        names['power_slice_' + str(i) + "_f_4"] = models.DecimalField(verbose_name="第" + str(i) + "个点负荷预测值_算法4", blank=True, null=True, max_digits=5, decimal_places=2)
        names['power_slice_' + str(i) + "_f_5"] = models.DecimalField(verbose_name="第" + str(i) + "个点负荷预测值_算法5", blank=True, null=True, max_digits=5, decimal_places=2)

        names['power_slice_' + str(i) + "_load"] = models.DecimalField(verbose_name="第" + str(i) + "个点负载率", blank=True, null=True, max_digits=5, decimal_places=2)
        names['power_slice_' + str(i) + "_load_f"] = models.DecimalField(verbose_name="第" + str(i) + "个点负荷率预测值", blank=True, null=True, max_digits=5, decimal_places=2)
        '''
        if i <= 89:
            # string fields use "" to store blank, therefore no need to add null=True
            names['power_slice_' + str(i) + "_load_next_status"] = \
                models.CharField(verbose_name="第" + str(i) + "个点到第" + str(i+7) + "个点的重过载实际状态", blank=True, max_length=255)
            names['power_slice_' + str(i) + "_load_next_status_f"] = \
                models.CharField(verbose_name="第" + str(i) + "个点到第" + str(i+7) + "个点的重过载预测状态", blank=True, max_length=255)
        '''
    load_status = models.SmallIntegerField(verbose_name="重过载实际状态", blank=True, null=True)
    load_status_f = models.SmallIntegerField(verbose_name="重过载预测状态", blank=True, null=True)

    max_time = models.SmallIntegerField(verbose_name="最大负荷对应点", blank=True, null=True)
    min_time = models.SmallIntegerField(verbose_name="最小负荷对应点", blank=True, null=True)

    load_average = models.DecimalField(verbose_name="平均负荷实际值", blank=True, null=True, max_digits=5, decimal_places=2)
    load_average_f = models.DecimalField(verbose_name="平均负荷预测值", blank=True, null=True, max_digits=5, decimal_places=2)
    load_max = models.DecimalField(verbose_name="最大负荷实际值", blank=True, null=True, max_digits=5, decimal_places=2)
    load_max_f = models.DecimalField(verbose_name="最大负荷预测值", blank=True, null=True, max_digits=5, decimal_places=2)
    load_min = models.DecimalField(verbose_name="最小负荷实际值", blank=True, null=True, max_digits=5, decimal_places=2)
    load_min_f = models.DecimalField(verbose_name="最小负荷预测值", blank=True, null=True, max_digits=5, decimal_places=2)

    # added on 1107
    peak_valley_difference = models.DecimalField(verbose_name="谷峰差", blank=True, null=True, max_digits=8, decimal_places=2)
    peak_valley_difference_f = models.DecimalField(verbose_name="谷峰差预测值", blank=True, null=True, max_digits=8,
                                                   decimal_places=2)

    load_rate = models.DecimalField(verbose_name="负载率", blank=True, null=True, max_digits=5, decimal_places=2)
    load_rate_min = models.DecimalField(verbose_name="最小负载率", blank=True, null=True, max_digits=5, decimal_places=2)

    load_rate_f = models.DecimalField(verbose_name="负载率预测值", blank=True, null=True, max_digits=5, decimal_places=2)
    load_rate_min_f = models.DecimalField(verbose_name="最小负载率预测值", blank=True, null=True, max_digits=5, decimal_places=2)

    load_f_accuracy = models.DecimalField(verbose_name="过去一个月累计负荷预测精度", blank=True, null=True, max_digits=8, decimal_places=2)
    # A,B,C,D,E

    precision_level = models.CharField(verbose_name="评价分析", blank=True, max_length=20, default='')

    phase_A_load_rate = models.DecimalField(verbose_name="A相负载率", blank=True, null=True, max_digits=5, decimal_places=2)
    phase_A_max = models.DecimalField(verbose_name="A相最大负荷", blank=True, null=True, max_digits=5, decimal_places=2)
    phase_B_load_rate = models.DecimalField(verbose_name="B相负载率", blank=True, null=True, max_digits=5, decimal_places=2)
    phase_B_max = models.DecimalField(verbose_name="B相最大负荷", blank=True, null=True, max_digits=5, decimal_places=2)
    phase_C_load_rate = models.DecimalField(verbose_name="C相负载率", blank=True, null=True, max_digits=5, decimal_places=2)
    phase_C_max = models.DecimalField(verbose_name="C相最大负荷", blank=True, null=True, max_digits=5, decimal_places=2)
    utilization_ratio = models.DecimalField(verbose_name="日利用率", blank=True, null=True, max_digits=5, decimal_places=2)

    class Meta:
        abstract = True


def fabric_daily(station_number_list, baseclass=TransformerLoadDailyBase):
    for station_number in station_number_list:
        class Meta:
            db_table = 'sys_transformer_daily_%s' % station_number
            verbose_name = '供电服务所（站）' + str(station_number) + '——台区日负荷表'
            verbose_name_plural = verbose_name
            unique_together = ["time", "transformer_number"]

        attrs = {'__module__': baseclass.__module__, 'Meta': Meta}
        newclass = type(str(station_number), (baseclass,), attrs)
        globals()[station_number] = newclass


def fabric_monthly(station_number_list, baseclass=TransformerLoadMonthlyBase):
    for station_number in station_number_list:
        class Meta:
            db_table = 'sys_transformer_monthly_%s' % station_number
            verbose_name = '供电服务所（站）' + str(station_number) + '——台区月负荷表'
            verbose_name_plural = verbose_name
            unique_together = ["month", "transformer_number"]

        attrs = {'__module__': baseclass.__module__, 'Meta': Meta}
        newclass = type(str(station_number), (baseclass,), attrs)
        globals()[str(station_number) + '_monthly'] = newclass

        '''
        # specify any other class attributes here. E.g. you can specify extra fields:
        attrs.update({'my_field': models.CharField(max_length=100)})
        newclass = type(str(station_number), (baseclass,), attrs)
        globals()[station_number] = newclass
        '''

# operator, create transformer load daily grouped by station number
station_list = []

for i in range(1, 11):
    new_station_number = 4000000 + i
    station_list.append(str(new_station_number))

fabric_daily(station_list)
# fabric_monthly(station_list)
print('process ends')


'''
    序号
    字段
    说明
    数据类型
    长度
    自增
    主键
    允许空
    默认值
    1
    id
    int        √    √    ×    NULL
    2
    name
    供电服务所（站）    varchar
    255    ×    ×    √    NULL
    3
    station_number
    供电服务所（站）编号
    int        ×    ×    ×    NULL
    4
    county_id
    所属区县编号
    int        ×    ×    ×    NULL
    5
    transformer_count
    台区数量
    int        ×    ×    ×    0
    6
    time
    时间
    datetime        ×    ×    ×    CURRENT_TIMESTAMP
    7
    day_of_week
    星期几
    int        ×    ×    ×    NULL
    8
    is_working_day
    是否为工作日
    tinyint        ×    ×    ×    NULL
    9
    energy_consumption
    用电量
    decimal        ×    ×    ×    NULL
    10
    energy_consumption_manual
    用电量人工修正值
    decimal        ×    ×    √    energy_consumption_f
    11
    energy_consumption_forecast
    用电量推荐预测值
    decimal        ×    ×    √    NULL
    12
    WOW_energy
    实际电量周同比
    decimal        ×    ×    √    0
    13
    WOW_energy_f
    预测电量周同比
    decimal        ×    ×    √    0
    13
    DOD_energy
    实际电量天环比
    decimal        ×    ×    √    0
    14
    DOD_energy_f
    预测电量天环比
    decimal        ×    ×    √    0
    15
    normal_count
    正常台区的实际数量
    int        ×    ×    √    NULL
    16
    overload_count
    过载台区的实际数量
    int        ×    ×    √    NULL
    17
    heavy_load_count
    重载台区的实际数量
    int        ×    ×    √    NULL
    18
    overload_count
    过载台区的实际数量
    int        ×    ×    √    NULL
    19
    overload_01_count
    轻度过载台区的实际数量
    int        ×    ×    √    NULL
    20
    overload_02_count
    中度过载台区的实际数量
    int        ×    ×    √    NULL
    21
    overload_03_count
    重度过载台区的实际数量
    int        ×    ×    √    NULL
    22
    overload_04_count
    异常过载台区的实际数量
    int        ×    ×    √    NULL
    18
    normal_count_f
    正常台区的预测数量
    int        ×    ×    √    NULL
    19
    overload_count_f
    过载台区的预测数量
    int        ×    ×    √    0
    20
    heavy_load_count_f
    重载台区的预测数量
    int        ×    ×    √    0
    21
    overload_01_count_f
    轻度过载台区的预测数量
    int        ×    ×    √    NULL
    22
    overload_02_count_f
    中度过载台区的预测数量
    int        ×    ×    √    NULL
    23
    overload_03_count_f
    重度过载台区的预测数量
    int        ×    ×    √    NULL
    24
    overload_04_count_f
    异常过载台区的预测数量
    int        ×    ×    √    NULL
    25
    power_slice_1
    第1个点（00：15）该供电服务所（站）下属台区的负荷值
    decimal        ×    ×    √    NULL
    26
    power_slice_1_f
    第1个点该供电服务所（站）下属台区的负荷预测值
    decimal        ×    ×    √    NULL
    27
    power_slice_1_manual
    第1个点该供电服务所（站）下属台区的负荷人工修正值
    decimal        ×    ×    √    power_slice_1_f
    28
    power_slice_1_load_status_00_count
    power_slice_1到power_slice_8该供电服务所（站）下属台区的正常台区的实际数量
    int        ×    ×    ×    NULL
    29
    power_slice_1_load_status_00_count_f
    power_slice_1到power_slice_8该供电服务所（站）下属台区的正常台区的预测数量
    int        ×    ×    ×    NULL
    30
    power_slice_1_load_status_01_count
    power_slice_1到power_slice_8该供电服务所（站）下属台区的重载台区的实际数量
    int        ×    ×    ×    NULL
    31
    power_slice_1_load_status_01_count_f
    power_slice_1到power_slice_8该供电服务所（站）下属台区的重载台区的预测数量
    int        ×    ×    ×    NULL
    32
    power_slice_1_load_status_02_count
    power_slice_1到power_slice_8该供电服务所（站）下属台区的轻过载台区的实际数量
    int        ×    ×    ×    NULL
    33
    power_slice_1_load_status_02_count_f
    power_slice_1到power_slice_8该供电服务所（站）下属台区的轻过载台区的预测数量
    int        ×    ×    ×    NULL
    34
    power_slice_1_load_status_03_count
    power_slice_1到power_slice_8该供电服务所（站）下属台区的中度过载台区的实际数量
    int        ×    ×    ×    NULL
    35
    power_slice_1_load_status_03_count_f
    power_slice_1到power_slice_8该供电服务所（站）下属台区的中度过载台区的预测数量
    int        ×    ×    ×    NULL
    36
    power_slice_1_load_status_04_count
    power_slice_1到power_slice_8该供电服务所（站）下属台区的重度过载台区的实际数量
    int        ×    ×    ×    NULL
    37
    power_slice_1_load_status_04_count_f
    power_slice_1到power_slice_8该供电服务所（站）下属台区的重度过载台区的预测数量
    int        ×    ×    ×    NULL
    38
    power_slice_1_load_status_05_count
    power_slice_1到power_slice_8该供电服务所（站）下属台区的异常过载台区的实际数量
    int        ×    ×    ×    NULL
    39
    power_slice_1_load_status_05_count_f
    power_slice_1到power_slice_8该供电服务所（站）下属台区的异常过载台区的预测数量
    int        ×    ×    ×    NULL
    40
    power_slice_1
    第2个点的负荷值（0：30）该供电服务所（站）下属台区的负荷值
    decimal        ×    ×    √    NULL
    …    …    …    decimal        ×    ×    √    NULL
    xxx
    power_slice_T
    第T个点的负荷值（24：00）    decimal        ×    ×    √    NULL
    xxx
    power_slice_T_f
    第T个点的负荷预测值
    decimal        ×    ×    √    NULL
    xxx
    power_slice_T_manual
    第T个点该供电服务所（站）下属台区的负荷人工修正值
    decimal        ×    ×    √    power_slice_T_f
    …    …    …    int        ×    ×    ×    NULL
    xxx
    sum
    今天该供电服务所（站）下属台区实际负荷加总值
    decimal        ×    ×    √    NULL
    xxx
    sum_f
    今天该供电服务所（站）下属台区预测负荷加总值
    decimal        ×    ×    √    NULL
    xxx
    sum_manual
    今天该供电服务所（站）下属台区人工修正加总值
    decimal        ×    ×    √    NULL
    xxx
    max
    今天该供电服务所（站）的实际最大负荷值
    decimal        ×    ×    √    NULL
    xxx
    max_f
    今天该供电服务所（站）的预测最大负荷值
    decimal        ×    ×    √    NULL
    xxx
    max_manual
    今天该供电服务所（站）的人工修正最大负荷值
    decimal        ×    ×    √    NULL
    xxx
    min
    今天该供电服务所（站）的实际最小负荷值
    decimal        ×    ×    √    NULL
    xxx
    min_f
    今天该供电服务所（站）的预测最小负荷值
    decimal        ×    ×    √    NULL
    xxx
    min_manual
    今天该供电服务所（站）的人工修正最小负荷值
    decimal        ×    ×    √    NULL
    xxx
    WOW
    实际负荷周同比
    decimal        ×    ×    √    0
    xxx
    WOW_f
    预测负荷周同比
    decimal        ×    ×    √    0
    xxx
    DOD
    实际负荷天环比
    decimal        ×    ×    √    0
    xxx
    DOD_f
    预测负荷天环比
    decimal        ×    ×    √    0
'''